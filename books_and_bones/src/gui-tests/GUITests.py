from sys import platform
import unittest
import time
from unittest import TestCase
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.ui import WebDriverWait, Select
from selenium.webdriver.support import expected_conditions as EC

class AcceptanceTests (TestCase):

	def setUp(self):
		self.homeurl = 'https://booksandbones.me/'
		if platform == "linux" or platform == "linux2":
			self.browser = webdriver.Chrome('./drivers/chromedriver_linux')
		elif platform == "darwin": # macOS
			self.browser = webdriver.Chrome('./drivers/chromedriver_macosx')
		elif platform == "win32":
			self.browser = webdriver.Chrome('./drivers/chromedriver.exe')
		self.browser.get(self.homeurl)
		self.browser.maximize_window()

	# Tests for correct homepage
	def test1(self):
		self.browser.get(self.homeurl)
		self.assertIn('Books and Bones', self.browser.title)
		self.browser.close()

	# Tests clickable home link on nav bar
	def test2(self):
		try:
			self.browser.find_element_by_link_text('Books and Bones').click()
			self.assertEqual("https://booksandbones.me/", self.browser.current_url)
		except NoSuchElementException:
			self.fail("The element doesn't exist")
		self.browser.close()

	# Tests Books and Bones header is right
	def test3(self):
		self.browser.find_element_by_link_text('Books and Bones').click()
		given = self.browser.find_element_by_class_name("container").text
		self.assertIn('Books and Bones', given)
		self.browser.close()

	# Tests libraries link works
	def test4(self):
		try:
			self.browser.find_element_by_link_text('libraries').click()
			self.assertEqual("https://booksandbones.me/libraries", self.browser.current_url)
		except NoSuchElementException:
			self.fail("The element doesn't exist")
		self.browser.close()

	# Tests Libraries header is right
	def test5(self):
		self.browser.find_element_by_link_text('libraries').click()
		given = self.browser.find_element_by_class_name("my-4").text
		self.assertIn('Libraries', given)
		self.browser.close()

	# Tests museums link works
	def test6(self):
		try:
			self.browser.find_element_by_link_text('museums').click()
			self.assertEqual("https://booksandbones.me/museums", self.browser.current_url)
		except NoSuchElementException:
			self.fail("The element doesn't exist")
		self.browser.close()

	# Tests Museum header is right
	def test7(self):
		self.browser.find_element_by_link_text('museums').click()
		given = self.browser.find_element_by_class_name("my-4").text
		self.assertIn('Museums', given)
		self.browser.close()

	# Tests events link works
	def test8(self):
		try:
			self.browser.find_element_by_link_text('events').click()
			self.assertEqual("https://booksandbones.me/events", self.browser.current_url)
		except NoSuchElementException:
			self.fail("The element doesn't exist")
		self.browser.close()

	# Tests Events header is right
	def test9(self):
		self.browser.find_element_by_link_text('events').click()
		given = self.browser.find_element_by_class_name("my-4").text
		self.assertIn('Events', given)
		self.browser.close()

	# Tests about link works
	def test10(self):
		try:
			self.browser.find_element_by_link_text('about').click()
			self.assertEqual("https://booksandbones.me/about", self.browser.current_url)
		except NoSuchElementException:
			self.fail("The element doesn't exist")
		self.browser.close()

	# Tests About header is correct
	def test11(self):
		self.browser.find_element_by_link_text('about').click()
		given = self.browser.find_element_by_class_name("my-4").text
		self.assertIn('About Books and Bones', given)
		self.browser.close()

	# Tests gitlab link works
	def test12(self):
		self.browser.find_element_by_link_text('about').click()
		try:
			self.browser.find_element_by_id('GitLab Repository').click()
			self.assertEqual("https://gitlab.com/bstroud/books-and-bones", self.browser.current_url)
		except NoSuchElementException:
			self.fail("The element doesn't exist")
		self.browser.close()

	#Test search all page works
	def test13(self):
		time.sleep(1)
		try:
			searchBar = self.browser.find_element_by_id("searchbar")
			searchBar.click()
			searchBar.send_keys("branch")
			self.browser.find_element_by_id('searchbarbutton').click()
			self.assertEqual("https://booksandbones.me/searchall/?search=branch", self.browser.current_url)
		except NoSuchElementException:
			self.fail("The element does not exist")
		self.browser.close()

	#Test search all page works
	def test14(self):
		time.sleep(1)
		try:
			searchBar = self.browser.find_element_by_id("searchbar")
			searchBar.click()
			searchBar.send_keys("alamo")
			self.browser.find_element_by_id('searchbarbutton').click()
			self.assertEqual("https://booksandbones.me/searchall/?search=alamo", self.browser.current_url)
		except NoSuchElementException:
			self.fail("The element does not exist")
		self.browser.close()

	#Test search all page works
	def test15(self):
		time.sleep(1)
		try:
			searchBar = self.browser.find_element_by_id("searchbar")
			searchBar.click()
			searchBar.send_keys("homeschool")
			self.browser.find_element_by_id('searchbarbutton').click()
			self.assertEqual("https://booksandbones.me/searchall/?search=homeschool", self.browser.current_url)
		except NoSuchElementException:
			self.fail("The element does not exist")
		self.browser.close()

	# Test search for event page works
	def test16(self):
		self.browser.find_element_by_link_text('events').click()
		time.sleep(1)
		searchBar = self.browser.find_element_by_class_name("search")
		searchBar.click()
		searchBar.send_keys("homeschool")
		self.browser.find_element_by_class_name("search").click()
		self.assertEqual("https://booksandbones.me/events", self.browser.current_url)
		self.browser.close()

		# Test search for museum page works
	def test17(self):
		self.browser.find_element_by_link_text('museums').click()
		time.sleep(1)
		searchBar = self.browser.find_element_by_class_name("search")
		searchBar.click()
		searchBar.send_keys("alamo")
		self.browser.find_element_by_class_name("search").click()
		self.assertEqual("https://booksandbones.me/museums", self.browser.current_url)
		self.browser.close()

	# Test search for library page works
	def test18(self):
		self.browser.find_element_by_link_text('libraries').click()
		time.sleep(1)
		searchBar = self.browser.find_element_by_class_name("search")
		searchBar.click()
		searchBar.send_keys("branch")
		self.browser.find_element_by_class_name("search").click()
		self.assertEqual("https://booksandbones.me/libraries", self.browser.current_url)
		self.browser.close()

	# Tests filtering
	def test19(self):
		self.browser.find_element_by_link_text('events').click()
		time.sleep(1)
		filter_button = self.browser.find_element_by_class_name("dropdown-button") 
		filter_button.click()
		time.sleep(1)
		selection = self.browser.find_element_by_link_text("Monday")
		selection.click()

	# Tests filtering
	def test20(self):
		self.browser.find_element_by_link_text('museums').click()
		time.sleep(1)
		filter_button = self.browser.find_element_by_class_name("dropdown-button") 
		filter_button.click()
		time.sleep(1)
		selection = self.browser.find_element_by_link_text("Alamo Heights")
		selection.click()

	# Tests filtering
	def test21(self):
		self.browser.find_element_by_link_text('libraries').click()
		time.sleep(1)
		filter_button = self.browser.find_element_by_class_name("dropdown-button") 
		filter_button.click()
		time.sleep(1)
		selection = self.browser.find_element_by_link_text("Open")
		selection.click()

	# Tests filtering
	def test22(self):
		self.browser.find_element_by_link_text('libraries').click()
		time.sleep(1)
		filter_button = self.browser.find_element_by_class_name("dropdown-button") 
		filter_button.click()
		time.sleep(1)
		selection = self.browser.find_element_by_link_text("Open")
		selection.click()
		time.sleep(1)
		reset_button = self.browser.find_element_by_class_name("reset") 
		filter_button.click()




	def endBrowser(self):
		self.browser.quit()

if __name__ == "__main__":
	unittest.main()
