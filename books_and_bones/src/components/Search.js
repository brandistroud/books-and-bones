//modified from https://scotch.io/tutorials/build-custom-pagination-with-react
//modified from https://react-native-training.github.io/react-native-elements/docs/searchbar.html

import React from 'react';

class Search extends React.Component {

  constructor(props) {
    super(props);
    this.state = { searchquery: '' };

    this.onButtonPush = this.onButtonPush.bind(this);
    this.onKeyDown = this.onKeyDown.bind(this);
  }

  onButtonPush(props){
        var data = this.search.value;
        if (data !== "") {
          this.props.updateSearch(data)
        }
    }

  onKeyDown(props){
        if(props.keyCode == 13) { // 13 is the key code for Enter
            var data = this.search.value;
            if (data !== "") {
              this.props.updateSearch(data)
            }
        }
    }

  render(){

        const { searchquery } = this.state;

        return (
            <div>
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
            <input ref={input => this.search = input} onKeyDown={this.onKeyDown} type="text" id="modelsearchbar" className="search" placeholder="Search" name="search"/>
            <button id="modelsearchbutton" onClick={this.onButtonPush} className="search" type="submit"><i class="fa fa-search"></i></button>
            </div>
        )
    }
}

export default Search;
