import React from "react";
import { Link } from "react-router-dom";
import Pagination from "./Pagination";
import { Button, ButtonToolbar, DropdownButton, Dropdown } from "react-bootstrap";
import Search from "./Search"
import ReactDOM from "react-dom";
import Highlighter from "react-highlight-words";

class Museums extends React.Component{
    constructor(props) {
        super(props)
        this.state = { instancesPerPage: 9,
        data: [], 
        currentPage: null, 
        totalPages: null, 
        refresh: true, 
     	activeQueries: {},
        activeSearch: ''}
        this.updateFilter = this.updateFilter.bind(this)
        this.updateSearch = this.updateSearch.bind(this)
        this.getFilterString = this.getFilterString.bind(this)
        this.reset = this.reset.bind(this)
    }

    onPageChanged = data => {
    	this.setState({'refresh':false})
        const { currentPage, totalPages, pageLimit, totalRecords } = data;

        var offset=(currentPage-1)*pageLimit
        var targetUrl = 'https://api.booksandbones.me/museums/?format=json'
        fetch(targetUrl+"&offset="+offset+"&search="+this.state.activeSearch+"&limit="+pageLimit+this.getFilterString())
            .then(response => response.json())
            .then(response => {
                const data=response['results']
                this.setState({ currentPage, data, totalPages });
            })
    
        };


    componentDidMount() {
        this.reset()
    }

    updateFilter(category, entry){
    	var actives = {...this.state.activeQueries}
    	//update active queries
    	switch (filtering[category]){
    		case "city":
    			actives.city = entry;
    			break;
    		case "county":
    			actives.county = entry;
    			break;
    		case "mus_type":
    			actives.mus_type = entry;
    			break;
    	}
        if(category === 'Sort by Name'){
            var sort = ""
            switch (entry) {
            case "Ascending":
                sort = "name";
                break;
            case "Descending":
                sort = "-name";
                break;
            }
            actives.sort = sort;
            this.setState({'activeQueries':actives}, () => {
            var home = 'https://api.booksandbones.me/libraries/?format=json&';
            var targetUrl =home+this.getFilterString()+"&search="+this.state.activeSearch 
            fetch(targetUrl)
                .then(response => response.json())
                .then(response => {
                    this.setState({'total':response['count'], 'refresh': true, 'activeQueries':actives })})
            });
        }else{
            this.setState({'activeQueries':actives}, () => {
            var targetUrl = 'https://api.booksandbones.me/museums/?format=json'
            fetch(targetUrl+this.getFilterString()+"&search="+this.state.activeSearch)
                .then(response => response.json())
                .then(response => {
                    this.setState({'total':response['count'], 'refresh': true, 'activeQueries':actives })})
            });
        }
    }

    getFilterString(){
    	var curFilters = ""
        for (var category in this.state.activeQueries) {
    		curFilters += "&" + category + "=" + this.state.activeQueries[category]
    	}
    	return curFilters
    }

    updateSearch(data){
        var targetUrl = 'https://api.booksandbones.me/museums/?format=json'
        fetch(targetUrl+"&search="+data+this.getFilterString())
            .then(response => response.json())
            .then(response => this.setState({'total':response['count'], 'activeSearch':data, 'refresh': true}))
    }

    reset(){
    	this.setState({'data':[]})
    	var targetUrl = 'https://api.booksandbones.me/museums/?format=json'
    	fetch(targetUrl)
            .then(response => response.json())
            .then(response => {
            	this.setState({'total':response['count'], 'refresh': true, 'activeQueries': {}, 'activeSearch':"" })})
    }

    render(){
        if(this.state.total){
            if (this.state.activeSearch !== "") {
                var printSearch = "Search: " + this.state.activeSearch;
            } else {
                var printSearch = "";
            }
            return   (
                    <div>
                         <div className="container">
                         <h1 className="my-4">Museums</h1>
                         <p>{printSearch}</p>
                         <div className="row">
                         <MuseumQuery func={this.updateFilter} activeQueries={this.state.activeQueries}/>
                         <p>&emsp;</p>
                         <Button className="reset" onClick={this.reset}>Reset</Button>
                         <p>&emsp;</p>
                         <Search updateSearch={this.updateSearch}/>
                         </div>
                         <br></br>
                         </div>
                         <div className="container">
                        <div className="row">
                        <div className="card-group">
                        {this.state.data.map(function(item, index){
                        return ( 
                            <PlaceCard
                                name={item.name}
                                id={item.id}
                                type={item.mus_type}
                                description={item.summary}
                                contact_info={item.contact_info}
                                image={item.image_url}
                                activeSearch={this.state.activeSearch}
                            />)
                            }, this)
                        }
                        </div>
                        </div>
                        </div>
                        <br></br>
                    <div className="container">
                    <div className="row">
                        <Pagination 
                            totalRecords={this.state.total} 
                            pageLimit={this.state.instancesPerPage} 
                            pageNeighbours={1} 
                            onPageChanged={this.onPageChanged}
                            refresh={this.state.refresh}/>
                    </div>
                    </div>
                    <br></br>
                    </div>
                 );
            } else {
             return(
                    <div>
                    <div className="container">
                     <h1 className="my-4">Museums</h1>
                     <MuseumQuery func={this.updateFilter} activeQueries={this.state.activeQueries}/>
                     <p>Search: {this.state.activeSearch}</p>
                     <p>No results found</p>
                     <Button className="reset" onClick={this.reset}>Reset</Button>
                     </div>
                     <br></br>
                     </div>
                     )
            }
    }
}

function PlaceCard(props){
	if (props.activeSearch === "") {
	    //check empty images
	    var image=props.image
	    if (props.image===""){
	        image="https://brusselsmuseums.imgix.net/covers/Collections-permanentes-du-Musee-dIxelles-Claude-Marchal-Brussels-2017.jpg"
	    }
	    return (
	        <div className="col-lg-4 col-sm-6 portfolio-item">
	            <div className="card h-100">
	                <img className="card-img-top" src={image} alt=""/>
	                <div className="card-body">
	                    <Link to={'/museum/'+props.id}><h4 className="card-title">{props.name}</h4></Link>
	                    <p className="card-text">{props.description.substr(0, 100)+"..."}</p>
	                    <p className="card-text">Category: {props.type}</p>
	                    <ContactInfo contact_info={props.contact_info[0]}/>
	                </div>
	            </div>
	        </div>
	    );
    //
	} else {
		if (props.description.toLowerCase().includes(props.activeSearch.toLowerCase())) {
            var index = props.description.toLowerCase().indexOf(props.activeSearch.toLowerCase());
            var len = props.activeSearch.length
            if(index < 50){
                var beg = ""
                var new_index = 0;
            } else {
                var beg = "..."
                var new_index = index - 50;
            }
            var description = beg + props.description.substr(new_index, len + 100) + "...";
        } else {
            var description = props.description.substr(0, 100)+"...";
        }

            return (
                <div className="col-lg-10 col-sm-10">
                    <div className="card h-100">
                        <div className="card-body">
                            <Link to={'/museum/'+props.id}><p className="card-title"><Highlighter
                            highlightClassName="YourHighlightClass"
                            searchWords={props.activeSearch.split(" ")}
                            autoEscape={true}
                            textToHighlight={props.name}/></p></Link>

                            <p className="card-text"><Highlighter
                            highlightClassName="YourHighlightClass"
                            searchWords={props.activeSearch.split(" ")}
                            autoEscape={true}
                            textToHighlight={description}/></p>

                            <p className="card-text">Category: <Highlighter
                            highlightClassName="YourHighlightClass"
                            searchWords={props.activeSearch.split(" ")}
                            autoEscape={true}
                            textToHighlight={props.type}/></p>

                            <SearchContactInfo contact_info={props.contact_info[0]} activeSearch={props.activeSearch}/>
                        </div>
                    </div>
                </div>
            )
        }
}

function SearchContactInfo(props){

    return(
        <div>
        <p className="card-text">City: <Highlighter
            highlightClassName="YourHighlightClass"
            searchWords={props.activeSearch.split(" ")}
            autoEscape={true}
            textToHighlight={props.contact_info.address_city}/></p>
        <p className="card-text">County: <Highlighter
            highlightClassName="YourHighlightClass"
            searchWords={props.activeSearch.split(" ")}
            autoEscape={true}
            textToHighlight={props.contact_info.address_county}/></p>
        </div>
        );
}

function ContactInfo(props){
    return(
        <div>
        <p className="card-text">City: {props.contact_info.address_city}</p>
        <p className="card-text">County: {props.contact_info.address_county}</p>
        <p className="card-text">State: {props.contact_info.address_state}</p>
        </div>
        );
}

class MuseumQuery extends React.Component{
     constructor(props) {
        super(props)
        this.setState({ activeQueries: props.activeQueries})
    }

   	componentWillReceiveProps(nextProps) {
  		this.setState({ activeQueries: nextProps.activeQueries }); 
	}

    checkTitle(variant){
    	for (var category in this.state.activeQueries) {
    		if (category === filtering[filtering[variant]]){
    			return this.state.activeQueries[category]
    		}
    	}
    	return variant
    }

    render(){
    	if (this.state){
        return(
        <div>
        <ButtonToolbar>
        {['City', 'County', 'Type', 'Sort by Name'].map(
            variant => (
            <DropdownButton
            className="dropdown-button"
            title={this.checkTitle(variant)}
            variant={variant.toLowerCase()}
            id={`dropdown-variants-${variant}`}
            key={variant}
            >
                <DropdownMenu category={variant} func={this.props.func} active={this.state.activeQueries}/>

            </DropdownButton> 
            ),
        )}
        </ButtonToolbar>
        </div>);}
        return(<div></div>)
    }
}

class DropdownMenu extends React.Component{
	constructor(props){
		super(props)
		this.state={}
	}

	componentWillReceiveProps(nextProps) {
  		this.setState({ activeQueries: nextProps.active }, () => this.renderPosts())
	}

	componentDidMount() {
        this.setState({ activeQueries: this.props.active }, () => this.renderPosts())

    }

    getFilterString(){
    	if(this.state){
    	var curFilters = ""
        for (var category in this.state.activeQueries) {
    		curFilters += "&" + category + "=" + this.state.activeQueries[category]
    	}
    	return curFilters
    } return ""
    }

	renderPosts = async() => {
        if(this.props.category==='Sort by Name'){
            this.setState({'entries':["Ascending","Descending"]})
        }
	    else if(this.props.activeSearch){
            var targetUrl = 'https://api.booksandbones.me/museum-distinct?field='+filtering[this.props.category]+this.getFilterString()+'&search='+this.props.activeSearch+'&format=json';
            fetch(targetUrl)
            .then(response => response.json())
            .then(response => this.setState({'entries':response }));
	    }
	    else{
	        var targetUrl = 'https://api.booksandbones.me/museum-distinct?field='+filtering[this.props.category]+this.getFilterString()+'&format=json'
            fetch(targetUrl)
            .then(response => response.json())
            .then(response => this.setState({'entries':response }));
	    }

	}

	render(){
		if (this.state && this.state.entries){
    		return(
    			<div> 
    			{this.state.entries.map(function (entry) {
                    return ( 
                       <Dropdown.Item onClick={() => this.props.func(filtering[this.props.category], entry)}>{entry}</Dropdown.Item> )
                    }, this)
                }
        		</div>
        	)
    	}
        return (<div></div>)
       }
}

const filtering = {
    'City':"contact_info__address_city", 
	'contact_info__address_city':'city',
	'County':"contact_info__address_county",
	"contact_info__address_county":'county',
    'Type':'mus_type',
    'Sort by Name':'Sort by Name',
	'mus_type':'mus_type' }

export default Museums;
