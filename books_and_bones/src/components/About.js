import React from "react";
import $ from 'jquery';
import './About.css'

function AboutDescription() {
  return (
    <div className="container">
    <div className="row">
      <div class="col-md-6">
        <br/><br/>
        <h2 className="my-4">About Books and Bones</h2>
        <p className="center">This project is to encourage the use and support of 
        local educational establishments. This project is double-sided, because it 
        promotes education within the community (the users), while also supporting 
        these very important entities (museums and libraries). Places like museums 
        and libraries need attendance in order to stay open. Community involvement 
        is what allows these places to flourish. For the user, they are able to access 
        resources that enrich them academically. We, as students of this university, 
        are extremely privileged to have such a great education, but that is not the case 
        for everyone. Museums and libraries are really important tools for those who want to learn more, but do not have the same opportunities as a college student. This site connects users to these educational entities by making it easy to see what is in their area, during the times that they are free.</p>
      </div>
      <div class="col-md-6">
        <img className="img-fluid rounded" src="images/about_dino.jpg" alt=""/>
      </div>
      </div>
    </div>
    )
}

function AboutData() { 
  return (
    <div className="container">
    <div className="row">
      <div class="col-md-6">
        <br/><br/>
        <img className="img-fluid rounded" src="images/about_books.jpg" alt=""/>
    </div>
    <div class="col-md-6">
        <h2>Our Data Integration</h2>
        <p>Our data is organized into three models: </p>
        <p>Libraries - currently for those located in the Austin area. Each has location, description, hours, contact information, a picture, and events. </p>
        <p>Museums - currently for those located in Central Texas. Each has location, description, hours, contact information, a picture, and events. </p>
        <p>Events - events with pictures and information for each of the libraries and museums. Also has related pages and types of events. </p>
      </div>
    </div>
    <br></br>
    </div>
  )
}

function DataSourcesAndScraping(){
  return (
  <div className="container">
    <h2>Our Data Sources and Scraping</h2>
        <p><a href="https://data.austintexas.gov/resource/wzca-8n69.json">data.austintexas.gov API:</a> Used to obtain list of Austin libraries as well as their locations, hours, and some descriptions. Returns JSON data. </p>
        <p><a href="https://www.mediawiki.org/wiki/REST_API">Wikipedia RESTful API:</a> Used for list of Austin museums as well as descriptions and images. Returns JSON data.</p>
        <p><a href="https://do512.com/">Do512 RESTful API:</a> Obtain list of upcoming events around Austin and their attributes. Returns JSON data.</p>
    </div>
  )
}

function Tools() {
  return (
  <div className="container">
      <div class="column">
        <h2>Our Tools</h2>
        <p><img src="images/bootstrap.png" height="20" width="20"/><a href="https://getbootstrap.com/docs/4.1/getting-started/introduction/">Bootstrap</a>: Front-End</p>
        <p><img src="images/aws.png" height="20" width="20"/><a href="https://docs.aws.amazon.com/#lang/en_us">AWS</a>: Webhosting</p>
        <p><img src="images/postman.jpg" height="20" width="20"/><a href="https://learning.getpostman.com/docs/postman_for_publishers/public_api_docs/">Postman</a>: API</p>
        <p><img src="images/namecheap.png" height="20" width="20"/><a href="https://www.namecheap.com/">NameCheap</a>: URL</p>
        <p><img src="images/gitlab.png" height="20" width="20"/><a href="https://about.gitlab.com/">GitLab</a>: Version Control</p>
        <p><img src="images/gmaps.png" height="20" width="20"/><a href="https://developers.google.com/maps/documentation/">Google Maps</a>: Location Services</p>
        <p><img src="images/twitter.png" height="20" width="20"/><a href="https://developer.twitter.com/en/docs.html">Twitter API</a>: Twitter Feed</p>
      </div>
    </div>
  )
}

function Links(){
  return (
      <div className="row">
      <div className="column" >
        <a id="GitLab Repository" title="GitLab Repository" href="https://gitlab.com/bstroud/books-and-bones"><img src="images/gitlab.png" alt=""/></a>
        </div>
      <div className="column" >
        <a title="Postman API" href="https://documenter.getpostman.com/view/2861176/S11NLwXE"><img src="images/postman.jpg" height="100" width="100" alt=""/></a>
      </div>
    </div>
  )
}

function Group(props) {
	return (
		<div>
    <AboutDescription/>
    <AboutData/>
    <div className="container">
    <br></br>
    <h2>Our Team</h2>
		<div className="row">
	<PeopleTile
		img = "images/sahil.png"
		name = "Sahil Mittal"
		year = "Junior"
		responsibilities = "Back-end, AWS"
		interests = "Computer Science and Marketing major who loves sports, tv, and video games."

	/>
	<PeopleTile
		img = "images/brandi.png"
		name = "Brandi Stroud"
		year = "Junior"
		responsibilities = "Front-End"
		interests = "Loves to sleep, but is majoring in Computer Science, minoring in Business, and certifying in Forensic Science."

	/>
	<PeopleTile
		img = "images/Laura.png"
		name = "Laura Shub"
		year = "Senior"
		responsibilities = "Front-End"
		interests = "Computer Science and Biochemistry major who likes cats and coffee."
	/>

	<PeopleTile
		img = "images/gil.png"
    name = "Gilberto Martinez"
		year = "Junior"
		responsibilities = "Back-end, API"
		interests = "I am a computer science major, minoring in Business. Interned in Seattle at Microsoft"

	/>
	<PeopleTile
		img = "images/rip.png"
		name = "Christopher Jones"
		year = "Junior"
		responsibilities = "N/A"
		interests = "Dropped the class."

	/>
  </div>
	</div>
		<Stats/>
    <DataSourcesAndScraping/>
    <Tools/>
    <Links/>
	</div>

	);
}

function PeopleTile(props) {
	return (
	 <div className="col-lg-4 col-sm-6 portfolio-item">
        <div className="card h-100 text-center">
          <img className="card-img-top" src={props.img} alt=""/>
          <div class="card-body">
            <h4 class="card-title">{props.name}</h4>
            <h6 class="card-subtitle mb-2 text-muted">{props.year}</h6>
            <h6 class="card-subtitle mb-2 text-muted">Major Responsibilities: {props.responsibilities}</h6>
            <p class="card-text">{props.interests} </p>
          </div>
       </div>
      </div>
	);
}

function Stats(){
    return (
    <div className="container">
    <h2>Our GitLab Stats</h2>
    <center className="table">
      <table styles="width:100%" bgcolor="#3A3D3C" text-align="center">
        <tr>
          <th><font color="white" id="team-members"/></th>
          <th><font color="white" id="commits"/></th>
          <th><font color="white" id="authored-issues"/></th>
          <th><font color="white" id="closed-issues"/></th>
          <th><font color="white" id="unit-tests"/></th>
        </tr>
        <tr>
          <td><font color="white" id="sahil-name"/></td>
          <td><font color="white" id="sahil-commit-num"/></td>
          <td><font color="white" id="sahil-opened-issue-num"/></td>
          <td><font color="white" id="sahil-closed-issue-num"/></td>
          <td><font color="white" id="sahil-tests"/></td>
        </tr>
        <tr>
          <td><font color="white" id="gil-name" text-align="justified-center"/></td>
          <td><font color="white" id="gil-commit-num"/></td>
          <td><font color="white" id="gil-opened-issue-num"/></td>
          <td><font color="white" id="gil-closed-issue-num"/></td>
          <td><font color="white" id="gil-tests"/></td>
        </tr>
        <tr>
          <td><font color="white" id="laura-name"/></td>
          <td><font color="white" id="laura-commit-num"/></td>
          <td><font color="white" id="laura-opened-issue-num"/></td>
          <td><font color="white" id="laura-closed-issue-num"/></td>
          <td><font color="white" id="laura-tests"/></td>
        </tr>
        <tr>
          <td><font color="white" id="chris-name"/></td>
          <td><font color="white" id="chris-commit-num"/></td>
          <td><font color="white" id="chris-opened-issue-num"/></td>
          <td><font color="white" id="chris-closed-issue-num"/></td>
          <td><font color="white" id="chris-tests"/></td>
        </tr>
        <tr>
          <td><font color="white" id="brandi-name"/></td>
          <td><font color="white" id="brandi-commit-num"/></td>
          <td><font color="white" id="brandi-opened-issue-num"/></td>
          <td><font color="white" id="brandi-closed-issue-num"/></td>
          <td><font color="white" id="brandi-tests"/></td>
        </tr>
        <tr>
          <td><font color="white" id="total-name"/></td>
          <td><font color="white" id="total-commit-num"/></td>
          <td><font color="white" id="total-opened-issue-num"/></td>
          <td><font color="white" id="total-closed-issue-num"/></td>
          <td><font color="white" id="total-tests"/></td>
        </tr>
      </table>
      <br/>
    </center>
    </div>
    );
}
class About extends React.Component {
  
  componentDidMount(){
    this.getHeaders();
    this.getNames();
    this.getCommitNumber();
    this.getClosedIssueCount();
    this.getTests();

  }
  render(){
    return (<Group/>);
  }
	


getHeaders(){
  var gitlabcommits = ("https://gitlab.com/api/v4/projects/10997366/repository/contributors");
  $.getJSON( gitlabcommits,)
      .done(function( data ) {
        countCommits(data)
        $("#team-members").text("Team Members")
        $("#commits").text("Commits")
        $("#authored-issues").text("Authored Issues")
        $("#closed-issues").text("Closed Issues")
        $("#unit-tests").text("Unit Tests")
    });
}

getNames(){
  //When we actually get around to making tests we can change this function to reflect that
  var gitlabcommits = ("https://gitlab.com/api/v4/projects/10997366/repository/contributors");
  $.getJSON( gitlabcommits,)
      .done(function( data ) {
        countCommits(data)
        $("#gil-name").text("Gilberto Martinez")
        $("#laura-name").text("Laura Shub")
        $("#sahil-name").text("Sahil Mittal")
        $("#brandi-name").text("Brandi Stroud")
        $("#chris-name").text("Christopher Jones")
        $("#total-name").text("Total") 
    });
}

getTests(){
    var gitlabcommits = ("https://gitlab.com/api/v4/projects/10997366/repository/contributors");
    $.getJSON( gitlabcommits,)
      .done(function( data ) {
        countCommits(data)
        $("#gil-tests").text("30")
        $("#laura-tests").text("33")
        $("#sahil-tests").text("30")
        $("#brandi-tests").text("22")
        $("#chris-tests").text("0")
        $("#total-tests").text("115")
  });
}

getCommitNumber(){
    var gitlabcommits = ("https://gitlab.com/api/v4/projects/10997366/repository/contributors");
    $.getJSON( gitlabcommits,)
      .done(function( data ) {
        var dict2 = countCommits(data)
        $("#gil-commit-num").text(dict2["Gilberto Martinez"]+dict2["Gil Martinez"])
        $("#sahil-commit-num").text(dict2["Sahil Mittal"]+dict2["sahilmit"])
        $("#brandi-commit-num").text(dict2["Brandi Stroud"])
        $("#laura-commit-num").text(dict2["laurashub"] + dict2["Laura Shub"])
        $("#chris-commit-num").text(dict2["tartarut"])
        $("#total-commit-num").text(data[0].commits+data[1].commits+data[2].commits+data[3].commits+data[4].commits+data[5].commits+data[6].commits)
    });
}
getClosedIssueCount(){
// https://gitlab.com/api/v4/projects/10997366/issues?state=closed/
    var gitlabissues = "https://gitlab.com/api/v4/projects/10997366/issues?per_page=100";
    $.getJSON( gitlabissues,)
      .done(function( data ) {
        var dict = countIssues(data);

        $("#gil-opened-issue-num").text(dict["Gilberto Martinez"][0])
        $("#sahil-opened-issue-num").text(dict["Sahil Mittal"][0])
        $("#brandi-opened-issue-num").text(dict["Brandi Stroud"][0])
        $("#laura-opened-issue-num").text(dict["laurashub"][0])
        $("#chris-opened-issue-num").text(dict["Chris"][0])
        $("#gil-closed-issue-num").text(dict["Gilberto Martinez"][1])
        $("#sahil-closed-issue-num").text(dict["Sahil Mittal"][1])
        $("#brandi-closed-issue-num").text(dict["Brandi Stroud"][1])
        $("#laura-closed-issue-num").text(dict["laurashub"][1])
        $("#chris-closed-issue-num").text(dict["Chris"][1])
        $("#total-opened-issue-num").text(dict["total"])
        $("#total-closed-issue-num").text(dict["total_closed"])

    });
}


}
function countIssues(data){
  var dict = {};
  data.forEach(element => {

      if(dict[element.author.name]==null){
        dict[element.author.name] = [1,0]
      }else{
        dict[element.author.name] = [dict[element.author.name][0]+1,dict[element.author.name][1]]
      }
      if(element.closed_by!=null){
        if(dict[element.closed_by.name]==null){
          dict[element.closed_by.name] = [0,1]
        }else{
          dict[element.closed_by.name] = [dict[element.closed_by.name][0],dict[element.closed_by.name][1]+1]
        }
        dict["total_closed"] = (dict["total_closed"]||0)+1
      }

      dict["total"] = (dict["total"]||0)+1

  });
  return dict;
}

function countCommits(data){
  var dict = {};
  dict["sahilmit"]= 0
  dict["Sahil Mittal"] = 0
  dict["Laura Shub"] = 0
  dict["laurashub"] = 0
  data.forEach(element => {
      dict[element.name] = (dict[element.name]||0)+element.commits
      dict["total"] = (dict["total"]||0)+element.commits
  });
  return dict;
}

export default About;
