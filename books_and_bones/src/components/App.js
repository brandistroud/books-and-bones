import React from "react";
import { BrowserRouter as Router, Route, NavLink } from "react-router-dom";
import Home from './Home';
import Libraries from './Libraries';
import Museums from './Museums';
import Events from './Events';
import About from './About'
import LibraryInstance from './LibraryInstance';
import EventInstance from './EventInstance';
import MuseumInstance from './MuseumInstance';
import Visualizations from './Visualizations.js'
import '../styles/App.css';
import './App.css'
import SearchAll from './SearchAll'

function App() {
  return (
    <Router>
      <div>
        <BnBNavbar />
        <Route exact path="/" component={Home} />
        <Route path="/searchall" component={SearchAll} />
        <Route path="/about" component={About} />
        <Route path="/libraries" component={Libraries} />
        <Route path="/museums" component={Museums} />
        <Route path="/events" component={Events} />
        <Route exact path="/library/:id" component={LibraryInstance} />
        <Route exact path="/visualizations" component={Visualizations} />
        <Route exact path="/event/:id" component={EventInstance} />
        <Route exact path="/museum/:id" component={MuseumInstance} />
        <footer className="py-5 bg-dark">
          <div className="container">
            <p className="m-0 text-center text-white">CS373: Software Engineering, 11 AM, Group 5</p>
          </div>
        </footer>
      </div>
    </Router>
  );
}

function BnBNavbar() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-dark navbar-fixed-top fixed-top">
      <a href="/"><img src="/images/logo.png" className="left" alt="logo.png" style={{ paddingRight: 5 }}></img></a>
      <a className="navbar-brand" href="/">Books and Bones</a>
      <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span id="nav" className="navbar-toggler-icon"></span>
      </button>

      <div className="collapse navbar-collapse" id="navbarSupportedContent">
        <ul className="navbar-nav mr-auto">
          <li className="nav-item">
            <NavLink to="/libraries" className="nav-link">Libraries</NavLink>
          </li>
          <li className="nav-item">
            <NavLink to="/museums" className="nav-link">Museums</NavLink>
          </li>
          <li className="nav-item">
            <NavLink to="/events" className="nav-link">Events</NavLink>
          </li>
          <li className="nav-item">
           <NavLink to="/visualizations" className="nav-link">Visualizations</NavLink>
          </li>
          <li className="nav-item">
            <NavLink to="/about" className="nav-link">About</NavLink>
          </li>
        </ul>
        <SearchBar />

      </div>
    </nav>
  );
}

class SearchBar extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      currentPage: 1,
      searchquery: ''
    };

    this.onButtonPush = this.onButtonPush.bind(this);
    this.onKeyDown = this.onKeyDown.bind(this);
  }

  componentDidMount() {
    this.gotoPage("", 1);
  }

  gotoPage = (search, currPage) => {
    const { onSearchMade = f => f } = this.props;

    const currentPage = 1; // currPage;

    const paginationData = {
      currentPage,
      totalPages: this.totalPages,
      pageLimit: this.pageLimit,
      totalRecords: this.totalRecords,
      search: search
    };

    this.setState({ currentPage }, () => onSearchMade(paginationData));
  }

  onButtonPush(props) {
    var data = this.search.value;
    if (data !== "") {
      window.location.href = '/searchall/?search=' + data;
    }
  }

  onKeyDown(props) {
    if (props.keyCode === 13) { // 13 is the key code for Enter
      var data = this.search.value;
      if (data !== "") {
        window.location.href = '/searchall/?search=' + data;
      }
    }
  }

  updateSearch = search => {
    this.setState({ search });
  };

  render() {
    return (
      <div className="form-inline my-2 my-lg-0">
        <input ref={input => this.search = input} onKeyDown={this.onKeyDown} type="text" id="searchbar" className="form-control mr-sm-2" placeholder="Search" name="search" />
        <button onClick={this.onButtonPush} id="searchbarbutton" className="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
      </div>
    )
  }
}

export default App;
