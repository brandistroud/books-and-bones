import { backendAPI }  from '../config.js'
import axios from 'axios';

export async function getLibraryPage(offset) {
    const response = await axios.get(`${backendAPI}libraries/?format=json&limit=12&offset=${offset}`);
    return response.data;
}
