import React from "react";
import { Carousel} from "react-bootstrap";


function Home() {
    return   (
        <div>
        <header>
            <div id="carouselExampleIndicators" className="carousel slide" data-ride="carousel">
            <ol className="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" className="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div className="carousel-inner" role="listbox">
                
                <Carousel.Item className="active">
                <img className="d-block w-100" src="images/dinosaur.jpg" alt="first slide"></img>
                </Carousel.Item>
                
                <Carousel.Item>
                <img className="d-block w-100" src="images/library.jpg" alt="second slide"></img>
                </Carousel.Item>
                
                <Carousel.Item>
                <img className="d-block w-100" src="images/aestheticbooks.jpg" alt="third slide"></img>
                </Carousel.Item>
            </div>
            <a className="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                <span className="sr-only">Previous</span>
            </a>
            <a className="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span className="carousel-control-next-icon" aria-hidden="true"></span>
                <span className="sr-only">Next</span>
            </a>
            </div>
        </header>
        <div className="container">
            <HomePageContent/>
        </div>
        </div>
    );
    }
function HomePageContent(){
    return (
        <div className="container">
            <h1 className="my-4">Supporting Educational Places and Events in Central Texas</h1>
            <div className="row">
                <ModelPortals header = "Places">
                    <p className="card-text">Check out <a href="/libraries">libraries</a> and <a href="/museums">museums</a> near you. They rely on your support to continue providing services!</p>
                </ModelPortals>
                <ModelPortals header = "Events">
                    <p className="card-text">Check out <a href="/events">events</a> near you. There are all types of events you can find, both entertaining and educational!</p>
                </ModelPortals>
                <ModelPortals header = "About">
                    <p className="card-text">Check out more information <a href="/about">about</a> Books and Bones. Let us support our local educational establishments!</p>
                </ModelPortals>
            </div>

        </div>
    );
    }

function ModelPortals(props){
    return (
        <div className="col-lg-4 mb-4">
            <div className="card h-100">
                <h4 className="card-header">{props.header}</h4>
                <div className="card-body">
                    {props.children}
                </div>
            </div>
        </div>
    );
    }

export default Home;
