import React from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Pagination from "./Pagination.js";
import { Button, ButtonToolbar, DropdownButton, Dropdown } from "react-bootstrap";
import Search from "./Search.js";
import ReactDOM from "react-dom";
import Highlighter from "react-highlight-words";

//get total in componentDidMount
//then set to 1 in pagination, will call populate card grid 

class Libraries extends React.Component{
	constructor(props) {
		super(props)
        this.state = { instancesPerPage: 9,
         data: [], 
         currentPage: null, 
         totalPages: null,
         refresh: true,
         activeQueries: {},
         activeSearch: "" }
        this.updateFilter = this.updateFilter.bind(this)
        this.getFilterString = this.getFilterString.bind(this)
        this.updateSearch = this.updateSearch.bind(this)
        this.reset = this.reset.bind(this)
    }
	

    onPageChanged = data => {
        this.setState({'refresh':false})
        const { currentPage, totalPages, pageLimit } = data;

        var offset=(currentPage-1)*pageLimit
        var targetUrl = 'https://api.booksandbones.me/libraries/?format=json'
        fetch(targetUrl+"&offset="+offset+"&search="+this.state.activeSearch+"&limit="+pageLimit+this.getFilterString())
            .then(response => response.json())
            .then(response => {
                const data=response['results']
                this.setState({ currentPage, data, totalPages });
            })
    
        };

    componentDidMount() {
        this.reset()
    };

    updateFilter(category, entry){
        var actives = {...this.state.activeQueries}
        //update active queries
        switch (filtering[category]){
            case "district":
                actives.district = entry;
                break;
            case "zip":
                actives.zip = entry;
                break;
            case "status":
                actives.status = entry;
                break;
        }
        if(category === 'Sort by Name'){
            var sort = ""
            switch (entry) {
            case "Ascending":
                sort = "name";
                break;
            case "Descending":
                sort = "-name";
                break;
            }
            actives.sort = sort;
            this.setState({'activeQueries':actives}, () => {
            var home = 'https://api.booksandbones.me/libraries/?format=json&';
            var targetUrl =home+this.getFilterString()+"&search="+this.state.activeSearch 
            fetch(targetUrl)
                .then(response => response.json())
                .then(response => {
                    this.setState({'total':response['count'], 'refresh': true, 'activeQueries':actives })})
            });
        }else{
            this.setState({'activeQueries':actives}, () => {
            var targetUrl = 'https://api.booksandbones.me/libraries/?format=json'
            fetch(targetUrl+this.getFilterString())
                .then(response => response.json())
                .then(response => this.setState({'total':response['count'], 'refresh': true, 'activeQueries':actives }))
            });
        }
    }

    updateSearch(data){
        var targetUrl = 'https://api.booksandbones.me/libraries/?format=json'
        fetch(targetUrl+"&search="+data+this.getFilterString())
            .then(response => response.json())
            .then(response => this.setState({'total':response['count'], 'activeSearch':data, 'refresh': true}))
    }

    getFilterString(){
        var curFilters = ""
        for (var category in this.state.activeQueries) {
            curFilters += "&" + category + "=" + this.state.activeQueries[category]
        }
        return curFilters
    }

    reset(){
        this.setState({'data':[]})
        var targetUrl = 'https://api.booksandbones.me/libraries/?format=json'
        fetch(targetUrl)
            .then(response => response.json())
            .then(response => {
                this.setState({'total':response['count'], 'refresh': true, 'activeQueries': {}, 'activeSearch':""  })})
    }


	render(){
        if(this.state.total){
            if (this.state.activeSearch !== "") {
                var printSearch = "Search: " + this.state.activeSearch;
            } else {
                var printSearch = "";
            }
            return   (
                <div>
                <div className="container">
                 <h1 className="my-4">Libraries</h1>
                 <p>{printSearch}</p>
                <div className="row">
                <LibraryQuery func={this.updateFilter} activeQueries={this.state.activeQueries}/>
                <p>&emsp;</p>
                <Button className="reset" onClick={this.reset}>Reset</Button>
                <p>&emsp;</p>
                <Search classNmae="search" updateSearch={this.updateSearch}/>
                </div>
                <br></br>
                </div>
                <div className="container">
                    <div className="row">

                    {this.state.data.map(function(item, index){
                    return ( 
                        <PlaceCard
                            name={item.name}
                            id={item.id}
                            contact_info={item.contact_info}
                            description={item.description}
                            open_hours={item.open_hours}
                            image={item.image_url}
                            district={item.district}
                            activeQueries={this.state.activeQueries}
                            activeSearch={this.state.activeSearch}
                        />)
                        }, this)
                    }
                    </div>
                    </div>
                    <div className="container">
                    <div className="row">
                        <Pagination 
                            totalRecords={this.state.total} 
                            pageLimit={this.state.instancesPerPage} 
                            pageNeighbours={5} 
                            onPageChanged={this.onPageChanged}
                            refresh={this.state.refresh}/>
                    </div>
                    </div>
                    <br></br>
                    </div>
                 );
         } else {
             return(
             <div>
             <div className="container">
             <h1 className="my-4">Libraries</h1>
             <p>Search: {this.state.activeSearch}</p>
             <p>No results found</p>
             <Button className="reset" onClick={this.reset}>Reset</Button>
             </div>
             <br></br>
             </div>
             )
         }
    } 
}

class LibraryQuery extends React.Component {
    constructor(props) {
        super(props)
        this.setState({ activeQueries: props.activeQueries})
    }

    componentWillReceiveProps(nextProps) {
        this.setState({ activeQueries: nextProps.activeQueries }); 
    }

    checkTitle(variant){
        for (var category in this.state.activeQueries) {
            if (category === filtering[filtering[variant]]){
                return this.state.activeQueries[category]
            }
        }
        return variant
    }

    render(){
        if (this.state){
        return (
            <div>
                <ButtonToolbar className="button-toolbar">
                {['Status', 'Zipcode', 'District', 'Sort by Name'].map(
                    variant => (
                       <DropdownButton
                        className="dropdown-button"
                        title={this.checkTitle(variant)}
                        variant={variant.toLowerCase()}
                        id={`dropdown-variants-${variant}`}
                        key={variant}
                    >
                <DropdownMenu category={variant} func={this.props.func} active={this.state.activeQueries}/>

            </DropdownButton> ))}
                </ButtonToolbar>
            </div>
        )} return(<div></div>)
    }
}

function PlaceCard(props){
    var status=`${getStatus(props.open_hours)}`
    if (!props.activeQueries.status || props.activeQueries.status === status){
    if ( props.activeSearch === "") {
        return (
            <div className="col-lg-4 col-sm-6 portfolio-item">
                <div className="card h-100">
                    <img className="card-img-top" src={props.image} alt=""/>
                    <div className="card-body">
                        <Link to={'/library/'+props.id}><h4 className="card-title">{props.name}</h4></Link>
                        <p className="card-text">{props.description.substr(0, 100)+"..."}</p>
                        <GetStatusText status={status}/>
                        <p className="card-text">District: {props.district}</p>
                        <ContactInfo contact_info={props.contact_info[0]}/>
                    </div>
                </div>
            </div>
        );
    } else {
        if (props.description.toLowerCase().includes( props.activeSearch.toLowerCase())) {
            var index = props.description.toLowerCase().indexOf( props.activeSearch.toLowerCase());
            var len =  props.activeSearch.length
            if(index < 50){
                var beg = ""
                var new_index = 0;
            } else {
                var beg = "..."
                var new_index = index - 50;
            }
            var description = beg + props.description.substr(new_index, len + 100) + "...";
        } else {
            var description = props.description.substr(0, 100)+"...";
        }

            return (
                <div className="col-lg-10 col-sm-10">
                    <div className="card h-100">
                        <div className="card-body">
                            <Link to={'/library/'+props.id}><Highlighter
                            highlightClassName="YourHighlightClass"
                            searchWords={ props.activeSearch.split(" ")}
                            autoEscape={true}
                            textToHighlight={props.name}/></Link>

                            <p className="card-text"><Highlighter
                            highlightClassName="YourHighlightClass"
                            searchWords={ props.activeSearch.split(" ")}
                            autoEscape={true}
                            textToHighlight={description}/></p>

                            <p className="card-text">District: <Highlighter
                            highlightClassName="YourHighlightClass"
                            searchWords={ props.activeSearch.split(" ")}
                            autoEscape={true}
                            textToHighlight={props.district}/></p>

                            <SearchContactInfo contact_info={props.contact_info[0]} activeSearch={props.activeSearch}/>
                        </div>
                    </div>
                </div>
            )
        }
    }
        return(<div></div>)
}

function SearchContactInfo(props){
    return(
        <div>
        <p className="card-text">City: <Highlighter
            highlightClassName="YourHighlightClass"
            searchWords={ props.activeSearch.split(" ")}
            autoEscape={true}
            textToHighlight={props.contact_info.address_city}/></p>

        <p className="card-text">Zipcode: <Highlighter
            highlightClassName="YourHighlightClass"
            searchWords={ props.activeSearch.split(" ")}
            autoEscape={true}
            textToHighlight={props.contact_info.address_zip}/></p>
        </div>
        );
}

function ContactInfo(props){
    return(
        <div>
        <p className="card-text">Phone: {props.contact_info.phone}</p>
        <p className="card-text">City: {props.contact_info.address_city}</p>
        <p className="card-text">State: {props.contact_info.address_state}</p>
        <p className="card-text">Zipcode: {props.contact_info.address_zip}</p>
        <p className="card-text"><a href={props.contact_info.website}>Website</a></p>
        </div>
        );
}

function getStatus(hours){
    var weekday_hours={}

    var split_hours=hours.split(",")
    for (var week_idx in split_hours) {
        if(!(split_hours[week_idx]==="")){
        var hours = split_hours[week_idx].split("y:")[1]
        weekday_hours[week_idx]={}
        if (!(hours === " Closed")){
            var tokens=hours.split(" ")
            weekday_hours[week_idx]['start'] = parseInt(tokens[1].split(":")[0])
            weekday_hours[week_idx]['end'] = parseInt(tokens[4].split(":")[0]) + 12
        }
         }
    }

    var d = new Date();
    var cur_hour=d.getHours()

    if (cur_hour >= weekday_hours[d.getDay()]['start'] && cur_hour < weekday_hours[d.getDay()]['end']){
        return("Open")
    }
    return("Closed")
}

function GetStatusText(props){
    if (props.status==="Open"){
        return (<p className="card-text">Status: <font color="green">Open</font></p>)
    }return(<p className="card-text">Status: <font color="red">Closed</font></p>)
}

class DropdownMenu extends React.Component{
    constructor(props){
        super(props)
        this.state={}
    }

    componentWillReceiveProps(nextProps) {
        this.setState({ activeQueries: nextProps.active }, () => this.renderPosts())
    }

    componentDidMount() {
        this.setState({ activeQueries: this.props.active }, () => this.renderPosts())

    }

    getFilterString(){
        if(this.state){
        var curFilters = ""
        for (var category in this.state.activeQueries) {
            curFilters += "&" + category + "=" + this.state.activeQueries[category]
        }
        return curFilters
    } return ""
    }

    renderPosts = async() => {
        if (this.props.category==="Status"){
            this.setState({'entries':["Open", "Closed"]})
        }else if(this.props.category==="Sort by Name"){
            this.setState({'entries':["Ascending","Descending"]})
        }else {
        var targetUrl = 'https://api.booksandbones.me/library-distinct?field='+filtering[this.props.category]+this.getFilterString()+'&format=json'
        fetch(targetUrl)
            .then(response => response.json())
            .then(response => this.setState({'entries':response }));
        }
    }

    render(){
        if (this.state && this.state.entries){
            return(
                <div> 
                {this.state.entries.map(function (entry) {
                    return ( 
                       <Dropdown.Item onClick={() => this.props.func(filtering[this.props.category], entry)}>{entry}</Dropdown.Item> )
                    }, this)
                }
                </div>
            )
        }
        return (<div></div>)
       }
}

const filtering = {'Status':"status", 
    'status':"status",
    'Zipcode':"contact_info__address_zip",
    "contact_info__address_zip":'zip',
    'Sort by Name':'Sort by Name',
    'District':'district',
    'district':'district' }

export default Libraries;
