from django.contrib.auth.models import User, Group
from rest_framework import viewsets
from api.serializer import LibrarySerializer, EventSerializer, MuseumSerializer
from api.models import Library, Event, Museum
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.views import APIView
from django.core.exceptions import FieldError
from api.filter import filterEvents, filterMuseums, filterLibraries


class LibraryViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Library.objects.all()
    serializer_class = LibrarySerializer

    def get_queryset(self):
        district = self.request.query_params.get("district", None)
        search = self.request.query_params.get("search", None)
        zipt = self.request.query_params.get("zip", None)
        sort = self.request.query_params.get("sort", None)

        params = {"district": district, "search": search, "zip": zipt, "sort": sort}
        return filterLibraries(self.queryset, params)


class EventViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = EventSerializer
    queryset = Event.objects.all()

    def get_queryset(self):
        return filterEvents(self.queryset, self.request)


class MuseumViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Museum.objects.all()
    serializer_class = MuseumSerializer

    def get_queryset(self):
        mus_type = self.request.query_params.get("mus_type", None)
        city = self.request.query_params.get("city", None)
        county = self.request.query_params.get("county", None)
        search = self.request.query_params.get("search", None)
        sort = self.request.query_params.get("sort", None)

        params = {
            "mus_type": mus_type,
            "city": city,
            "county": county,
            "search": search,
            "sort": sort,
        }
        return filterMuseums(self.queryset, params)


@api_view()
def getMuseumDistinctValues(request):
    field = request.query_params.get("field")
    if field == "city" or field == "county":
        field = "contact_info__address_" + field
    mus_type = request.query_params.get("mus_type", None)
    city = request.query_params.get("city", None)
    county = request.query_params.get("county", None)
    search = request.query_params.get("search", None)
    sort = request.query_params.get("sort", None)
    params = {
        "mus_type": mus_type,
        "city": city,
        "county": county,
        "search": search,
        "sort": sort,
    }
    queryset = filterMuseums(Museum.objects.all(), params)
    return Response(getGenericDistinctValues(queryset, field))


@api_view()
def getEventDistinctValues(request):
    field = request.query_params.get("field")
    if field == "city" or field == "zip":
        field = "place__contact_info__address_" + field
    queryset = filterEvents(Event.objects.all(), request)
    return Response(getGenericDistinctValues(queryset, field))


@api_view()
def getLibraryDistinctValues(request):
    field = request.query_params.get("field")
    if field == "zip" or field == "city":
        field = "contact_info__address_" + field

    district = request.query_params.get("district", None)
    search = request.query_params.get("search", None)
    zipt = request.query_params.get("zip", None)
    sort = request.query_params.get("sort", None)

    params = {"district": district, "search": search, "zip": zipt, "sort": sort}
    queryset = filterLibraries(Library.objects.all(), params)
    return Response(getGenericDistinctValues(queryset, field))


def getGenericDistinctValues(model, field_name):
    try:
        tm = map(
            lambda x: x[field_name],
            model.order_by(field_name).values(field_name).distinct(),
        )
    except FieldError:
        return list()
    return list(tm)
