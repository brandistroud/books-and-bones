def searchEvents(queryset, search):
    search = search.split(" ")
    result = queryset.filter(name__icontains=search)
    for word in search:
        queryset_temp = queryset.filter(name__icontains=word)
        queryset_temp2 = queryset.filter(description__icontains=word)
        queryset_temp3 = queryset.filter(place__name__icontains=word)
        queryset_temp4 = queryset.filter(pub_date__icontains=word)
        result = (
            queryset_temp | queryset_temp2 | queryset_temp3 | queryset_temp4 | result
        )
    return result


def searchLibraries(queryset, search):
    search = search.split(" ")
    result = queryset.filter(name__icontains=search)
    for word in search:
        queryset_temp = queryset.filter(name__icontains=word)
        queryset_temp2 = queryset.filter(description__icontains=word)
        queryset_temp3 = queryset.filter(contact_info__address_city__icontains=word)
        queryset_temp4 = queryset.filter(contact_info__address_zip__icontains=word)
        result = (
            queryset_temp | queryset_temp2 | queryset_temp3 | queryset_temp4 | result
        )
    return result


def searchMuseums(queryset, search):
    search = search.split(" ")
    result = queryset.filter(name__icontains=search)
    for word in search:
        queryset_temp = queryset.filter(name__icontains=word)
        queryset_temp2 = queryset.filter(summary__icontains=word)
        queryset_temp3 = queryset.filter(contact_info__address_city__icontains=word)
        queryset_temp4 = queryset.filter(contact_info__address_county__icontains=word)
        queryset_temp5 = queryset.filter(mus_type__icontains=word)
        result = (
            queryset_temp
            | queryset_temp2
            | queryset_temp3
            | queryset_temp4
            | queryset_temp5
            | result
        )
    return result
